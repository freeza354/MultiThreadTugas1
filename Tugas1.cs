﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace tugasMultithread1
{
    class MainThreadProgram
    {
        public static int inputConvert, inputConvert2 = new Int32();
        public static int hasil1 = 0, hasil2 = 0;
        public static float hasilPemb;
        public static string input;

        public static void HitungPembilang() {

            for (int i = inputConvert; i > 0; i--)
            {
                if (hasil1 == 0)
                    hasil1 = inputConvert;
                else
                {
                    hasil1 *= i;
                }
            }

        }

        public static void HitungPenyebut()
        {

            for (int i = inputConvert2; i > 0; i--)
            {
                if (hasil2 == 0)
                    hasil2 = inputConvert2;
                else
                {
                    hasil2 *= i;
                }
            }

        }

        public static void HitungPecahan()
        {
            Console.WriteLine("The fractions after factorial method used are : {0} / {1}", hasil1, hasil2);
        }

        public static void HitungHasil() {
            hasilPemb = hasil1 / hasil2;
            Console.WriteLine("The result of the factorial fractions are : {0} ", hasilPemb);
        }

        static void Main(string[] args)
        {
            Thread thread1 = new Thread(HitungPembilang);
            Thread thread2 = new Thread(HitungPenyebut);
            Thread thread3 = new Thread(HitungPecahan);
            Thread thread4 = new Thread(HitungHasil);
            Console.WriteLine("Threads Created!");

            Console.WriteLine("Input the numerator : ");
            input = Console.ReadLine();
            inputConvert = Convert.ToInt32(input);

            Console.WriteLine("Input the denominator : ");
            input = Console.ReadLine();
            inputConvert2 = Convert.ToInt32(input);

            thread1.Start();
            thread2.Start();

            thread1.Join();
            thread2.Join();
            Console.WriteLine("The threads for computing are already done. Hre's the result...");

            thread3.Start();
            thread4.Start();

            Console.WriteLine("Press any key to exit..");
            Console.ReadKey();
            
        }
    }
}
